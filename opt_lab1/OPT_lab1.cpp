﻿#include <iostream>
#include <string.h>
#pragma warning(disable : 4996)

using namespace std;

struct TSymbol
{
	char value;
	int attr;
};

int attributes[128];
TSymbol symbol;
int lexcode;
string buf;
bool suppressOutput;
FILE* FINP;
FILE* FOUT;
int row = 1;
int col = 0;

TSymbol getS()
{
	TSymbol res;
	fscanf(FINP, "%c", &res.value);
	res.attr = attributes[res.value];
	col++;
	if (res.value == '\n')
	{
		row++;
		col = 0;
	}
	return res;
}


void fill_attr()
{
	for (int i = 0; i < 128; i++)
	{
		if (i == 32 || i==13 || i == 10 || i == 9 || i == 11 || i == 12)//WHITESPACE
		{
			attributes[i] = 0;
		}
		else if (i >= '0' && i<= '9')//CONST
		{
			attributes[i] = 1;
		}
		else if (i >= 'A' && i <= 'Z')//IND
		{
			attributes[i] = 2;
		}
		else if (i == '(')//COMENT
		{
			attributes[i] = 3;
		}
		else if (i == ';' || i == ')' || i == ',')//розподільник
		{
			attributes[i] = 4;
		}
		else//ERROR
		{
			attributes[i] = 5;
		}

	}
}

struct TabSearchRes
{
	bool exist;
	int lexCode;
};

struct Tab 
{
	string name;
	int lexcode;
};

Tab KeyTab[400];
Tab ConstTab[1000];
Tab IndTab[1000];

int ConstCount = 0;
TabSearchRes ConstTabSearch(string buf)
{ 
	for (int i = 0; i < ConstCount; i++)
	{
		if (ConstTab[i].name == buf)
		{
			return { true, ConstTab[i].lexcode };
		}
	}
	return { false, ConstCount==0?1000000:ConstTab[ConstCount-1].lexcode };
}

void ConstTabForm(string buf)
{
	static int lexCode = 1000001;
	ConstTab[ConstCount].name = buf;
	ConstTab[ConstCount].lexcode = lexCode++;
	ConstCount++;
}

int KeyCount = 0;
TabSearchRes KeyTabSearch(string buf)
{
	for (int i = 0; i < KeyCount; i++)
	{
		if (KeyTab[i].name == buf)
		{
			return { true, KeyTab[i].lexcode };
		}
	}
	return { false, KeyTab[KeyCount-1].lexcode };
}

int IdnCount = 0;
TabSearchRes IdnTabSearch(string buf)
{
	for (int i = 0; i < IdnCount; i++)
	{
		if (IndTab[i].name == buf)
		{
			return { true, IndTab[i].lexcode };
		}
	}
	return { false, IdnCount==0?1000:IndTab[IdnCount-1].lexcode };
}

void IdnTabForm(string buf)
{
	static int lexCode = 1001;
	IndTab[IdnCount].name = buf;
	IndTab[IdnCount].lexcode = lexCode++;
	IdnCount++;
}

void showError(const char* error)
{
	fprintf(FOUT,"ERROR: %s\n", error);
}

void KeyTabForm()
{
	static int lexCode = 601;
	KeyTab[KeyCount].name = "PROCEDURE";
	KeyTab[KeyCount].lexcode = lexCode++;
	KeyCount++;
	KeyTab[KeyCount].name = "BEGIN";
	KeyTab[KeyCount].lexcode = lexCode++;
	KeyCount++;
	KeyTab[KeyCount].name = "END";
	KeyTab[KeyCount].lexcode = lexCode++;
	KeyCount++;
	KeyTab[KeyCount].name = "RETURN";
	KeyTab[KeyCount].lexcode = lexCode++;
	KeyCount++;
}

int main()
{
	KeyTabForm();
	fill_attr();
	printf("input name test directory:");
	char *dir = new char[100],*path = new char[100];
	scanf("%s", dir);
	sprintf(path, "%s\\%s", dir, "input.sig");
	FINP = fopen(path, "rt");
	sprintf(path, "%s\\%s", dir, "generated.txt");
	FOUT = fopen(path, "wt");
	bool need_to_read = true;
	int save_col;
	int save_row;
	fprintf(FOUT,"Output: %-15s lexcode\trow\tcol\n", "lex");
	while (!feof(FINP))
	{
		if (need_to_read)
		{
			symbol = getS();
			need_to_read = false;
		}
		save_col = col;
		save_row = row;
		buf = "";
		lexcode = 0;
		suppressOutput = false;
		switch (symbol.attr)
		{
			case 0://whitespace
			{
				while (!feof(FINP))
				{
					symbol = getS();
					if (symbol.attr != 0)
					{
						break;
					}
				}
				suppressOutput = true;
				break;
			}
			case 1://const
			{
				while (!feof(FINP) && symbol.attr == 1)
				{
					buf += symbol.value;
					symbol = getS();
				}
				TabSearchRes res = ConstTabSearch(buf);
				if (res.exist)
				{
					lexcode = res.lexCode;
				}
				else
				{
					lexcode = res.lexCode + 1;
					ConstTabForm(buf);
				}
				break;
			}
			case 2://ident
			{
				while (!feof(FINP) && (symbol.attr == 2 || symbol.attr == 1))
				{
					buf += symbol.value;
					symbol = getS();
				}
				TabSearchRes res = KeyTabSearch(buf);
				if (res.exist)
				{
					lexcode = res.lexCode;
				}
				else
				{
					TabSearchRes res = IdnTabSearch(buf);
					if (res.exist)
					{
						lexcode = res.lexCode;
					}
					else
					{
						lexcode = res.lexCode + 1;
						IdnTabForm(buf);
					}
				}
				break;
			}
			case 3://coment
			{
				if (feof(FINP))
				{
					buf = '(';
					lexcode = '(';
				}
				else
				{
					symbol = getS();
					if (symbol.value == '*')
					{
						if (feof(FINP))
						{
							showError("*) expected but end of file found");	
							suppressOutput = true;
						}
						else
						{
							symbol = getS();
							while (symbol.value != ')')
							{
								while (!feof(FINP) && symbol.value != '*')
								{
									symbol = getS();
								}
								if (feof(FINP))
								{
									showError("*) expected but end of file found");
									suppressOutput = true;
									symbol.value = '+';
									break;
								}
								else
								{
									symbol = getS();
								}
							}
							if (symbol.value == ')')
							{
								suppressOutput = true;
							}
							if (!feof(FINP))
							{
								symbol = getS();
							}
						}
					}
					else
					{
						buf = '(';
						lexcode = '(';
					}
				}
				break;
			}
			case 4://розподільник
			{
				buf += symbol.value;
				lexcode = symbol.value;
				symbol = getS();
				break;
			}
			case 5://error
			{
				showError("illegal sumbol");
				symbol = getS();
				suppressOutput = true;
				break;
			}	
		}
		if (!suppressOutput)
		{
			fprintf(FOUT,"Output: %-15s %d\t%d\t%d\n",buf.c_str(),lexcode,save_row,save_col);
		}
	}
	fclose(FINP);
	fclose(FOUT);
}
